#include "stdafx.h"
#include <iostream>
#include "zoo.h"

using namespace std;

zooShop::zooShop(){};
zooShop::~zooShop() {};

zooShop::zooShop(string newAnimal, string newGender, string newName, unsigned newCost, unsigned newCount)
{
	animal = newAnimal;
	gender = newGender;
	name = newName;
	cost = newCost;
	count = newCount;
};

void zooShop::setData(string newAnimal, string newGender, string newName, unsigned newCost, unsigned newCount)
{
	animal = newAnimal;
	gender = newGender;
	name = newName;
	cost = newCost;
	count = newCount;
};

void zooShop::getData()
{

	cout << "Animal: " << animal 
		 << "\nGender: " << gender
		 << "\nName: " << name 
		 << "\nCost: " << cost 
		 << "\nCount: " << count << endl;

};
void  zooShop::anotherCount(unsigned anCount)
{
	count = anCount;
}