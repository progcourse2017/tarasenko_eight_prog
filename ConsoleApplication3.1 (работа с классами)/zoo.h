#include <string>

using namespace std;


class zooShop
{
private:
		std::string animal;
		std::string gender;
		std::string name;
		unsigned cost;
		unsigned count;
public:
	zooShop();
	zooShop(string newAnimal, string newGender, string newName, unsigned newCost, unsigned newCount);
	zooShop(const zooShop &obj);
	~zooShop();

	void setData(string newAnimal, string newGender, string newName, unsigned newCost, unsigned newCount);
	void getData();

	void anotherCount(unsigned anCount);
};




